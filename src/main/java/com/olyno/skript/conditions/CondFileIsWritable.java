package com.olyno.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;

import java.nio.file.Files;
import java.nio.file.Path;

@Name("File is writable?")
@Description("Checks if the file is writable or not.")
@Examples({
	"command readable:\n" +
			"\ttrigger:\n" +
			"\t\tif file \"plugins/Skript/scripts/myAwesomeScript.sk\" is writable:\n" +
			"\t\t\tbroadcast \"Sure!\""
})
@Since("1.0")

public class CondFileIsWritable extends PropertyCondition<Path> {

	static {
		PropertyCondition.register(CondFileIsWritable.class,
				"writable", "path"
		);
	}

	@Override
	public boolean check(Path file) {
		return Files.isWritable(file);
	}

	@Override
	protected String getPropertyName() {
		return "writable";
	}
}
