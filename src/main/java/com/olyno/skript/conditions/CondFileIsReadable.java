package com.olyno.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;

import java.nio.file.Files;
import java.nio.file.Path;

@Name("File is readable?")
@Description("Checks if the file is readable or not.")
@Examples({
	"command readable:\n" +
			"\ttrigger:\n" +
			"\t\tif file \"plugins/Skript/scripts/myAwesomeScript.sk\" is readable:\n" +
			"\t\t\tbroadcast \"Sure!\""
})
@Since("1.0")

public class CondFileIsReadable extends PropertyCondition<Path> {

	static {
		PropertyCondition.register(CondFileIsReadable.class,
				"readable", "path"
		);
	}

	@Override
	public boolean check(Path file) {
		return Files.isReadable(file);
	}

	@Override
	protected String getPropertyName() {
		return "readable";
	}
}
