package com.olyno.skript.conditions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Condition;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.SyntaxElementInfo;
import ch.njol.util.Kleenean;
import com.olyno.skript.expressions.files.ExprFileDirectory;
import org.bukkit.event.Event;

import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

@Name("File or directory exists?")
@Description("Checks if the file or directory exists or not.")
@Examples({
	"command exists:\n" +
			"\ttrigger:\n" +
			"\t\tif file \"plugins/Skript/scripts/myAwesomeScript.sk\" exists:\n" +
			"\t\t\tbroadcast \"Awesome!\""
})
@Since("1.0")

public class CondFileExists extends Condition {

	static {
		try {
			registerCondition(CondFileExists.class,
					"[(1¦file[s])] %string/path% (exist[s]|(is|are) set)",
					"[(1¦file[s])] %string/path% (do[es](n't| not) exist|(is|are)(n't| not) set)"
			);

		} catch (NoSuchFieldException | IllegalAccessException ex) {
			Skript.exception(ex);
		}
	}

	private Expression<?> file;

	private static <E extends Condition> void registerCondition(final Class<E> condition, final String... patterns) throws NoSuchFieldException, IllegalAccessException {
		String originClassPath = Thread.currentThread().getStackTrace()[2].getClassName();
		final SyntaxElementInfo<E> info = new SyntaxElementInfo<>(patterns, condition, originClassPath);
		Field field = Skript.class.getDeclaredField("conditions");
		field.setAccessible(true);
		ArrayList o = (ArrayList) field.get(Skript.class);
		o.add(0, info);
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		if ((parseResult.mark == 1) && (expr[0] instanceof ExprFileDirectory)) return false;
		else if ((parseResult.mark == 0) && (!(expr[0] instanceof ExprFileDirectory))) return false;
		file = expr[0];
		setNegated(matchedPattern == 1);
		return true;
	}

	@Override
	public boolean check(Event e) {
		Object f = file.getSingle(e);
		Path path = f instanceof String ? Paths.get((String) f) : ((ExprFileDirectory) file).getSingle(e);
		if (isNegated()) {
			return !Files.exists(path);
		} else {
			return Files.exists(path);
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return file.toString(e, debug) + (isNegated() ? " doesn't exist" : " exists");
	}

}
