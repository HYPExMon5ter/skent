package com.olyno.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;

import java.nio.file.Files;
import java.nio.file.Path;

@Name("File is executable?")
@Description("Checks if the file executable or not.")
@Examples({
	"command executable:\n" +
			"\ttrigger:\n" +
			"\t\tif file \"plugins/Skript/scripts/myAwesomeGame.exe\" is executable:\n" +
			"\t\t\tbroadcast \"Oh, probably a game!\""
})
@Since("1.0")

public class CondFileIsExecutable extends PropertyCondition<Path> {

	static {
		PropertyCondition.register(CondFileIsExecutable.class,
				"[a[n]] exe[cutable]", "path"
		);
	}

	@Override
	public boolean check(Path file) {
		return Files.isExecutable(file);
	}

	@Override
	protected String getPropertyName() {
		return "executable";
	}
}
