package com.olyno.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;

import java.nio.file.Path;
import java.util.regex.Pattern;

@Name("File is a file?")
@Description("Checks if the file is a file or not.")
@Examples({
	"command file:\n" +
			"\ttrigger:\n" +
			"\t\tif file \"plugins/Skript/scripts/myAwesomeScript.sk\" is a file:\n" +
			"\t\t\tbroadcast \"Sure!\""
})
@Since("1.0")

public class CondFileIsFile extends PropertyCondition<Path> {

	static {
		PropertyCondition.register(CondFileIsFile.class,
				"[a] file", "path"
		);
	}

	@Override
	public boolean check(Path file) {
		return Pattern.compile("\\.\\w+$").matcher(file.toString()).find();
	}

	@Override
	protected String getPropertyName() {
		return "file";
	}
}
