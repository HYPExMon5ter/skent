package com.olyno.skript.conditions;

import ch.njol.skript.Skript;
import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Name("File is hidden?")
@Description("Checks if the file is hidden or not.")
@Examples({
		"command hidden:\n" +
				"\ttrigger:\n" +
				"\t\tif file \"plugins/twitch.txt\" is hidden:\n" +
				"\t\t\tbroadcast \"I'm sneeeaaaakyy!\""
})
@Since("1.0")

public class CondFileIsHidden extends PropertyCondition<Path> {

	static {
		PropertyCondition.register(CondFileIsHidden.class,
				"hidden", "path"
		);
	}

	@Override
	public boolean check(Path file) {
		if (Files.exists(file)) {
			try {
				return Files.isHidden(file);
			} catch (IOException ex) {
				Skript.exception(ex, "This file/directory doesn't exist: " + file);
			}
		}
		return false;
	}

	@Override
	protected String getPropertyName() {
		return "hidden";
	}
}
