package com.olyno.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;

import java.nio.file.Path;
import java.util.regex.Pattern;

@Name("File is a directory?")
@Description("Checks if the file is a directory or not.")
@Examples({
	"command dir:\n" +
			"\ttrigger:\n" +
			"\t\tif file \"plugins/Skript/scripts/myAwesomeScript.sk\" is a dir:\n" +
			"\t\t\tbroadcast \"Nah!\""
})
@Since("1.0")

public class CondFileIsDir extends PropertyCondition<Path> {

	static {
		PropertyCondition.register(CondFileIsDir.class,
				"[a] dir[ectory]", "path"
		);
	}

	@Override
	public boolean check(Path file) {
		return (!Pattern.compile("\\.\\w+$").matcher(file.toString()).find());
	}

	@Override
	protected String getPropertyName() {
		return "directory";
	}
}
