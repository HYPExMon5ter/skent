package com.olyno.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.olyno.skript.events.bukkit.MoveEvent;

import java.nio.file.Path;

@Name("Move File/Directory")
@Description("When a file or directory is moved.")
@Examples({
		""
})
@Since("1.0")

public class EvtMove {

	static {
		Skript.registerEvent("Move", SimpleEvent.class, MoveEvent.class,
				"(file|dir[ector(ies|y)]) mov(ed|ing|e)"
		);

		EventValues.registerEventValue(MoveEvent.class, Path.class, new Getter<Path, MoveEvent>() {
			@Override
			public Path get(MoveEvent e) {
				return e.getSource();
			}
		}, 0);

		EventValues.registerEventValue(MoveEvent.class, Path.class, new Getter<Path, MoveEvent>() {
			@Override
			public Path get(MoveEvent e) {
				return e.getTarget();
			}
		}, 0);

	}
}
