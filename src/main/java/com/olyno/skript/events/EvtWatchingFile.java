package com.olyno.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.SkriptEventHandler;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Literal;
import ch.njol.skript.lang.SelfRegisteringSkriptEvent;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.Trigger;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.olyno.skript.events.bukkit.WatchingFileEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.file.*;

@Name("Watching File/Directory")
@Description("Watching all changes of a directory or a file (save, delete...)")
@Examples({
		""
})
@Since("1.0")

public class EvtWatchingFile extends SelfRegisteringSkriptEvent {

	private static Path path;
	private static int taskID = -1;

	static {
		Skript.registerEvent("Watching file", EvtWatchingFile.class, WatchingFileEvent.class,
				"watching %file%"
		);

		EventValues.registerEventValue(WatchingFileEvent.class, String.class, new Getter<String, WatchingFileEvent>() {
			@Override
			public String get(WatchingFileEvent e) {
				return e.getKind();
			}
		}, 0);

		EventValues.registerEventValue(WatchingFileEvent.class, Path.class, new Getter<Path, WatchingFileEvent>() {
			@Override
			public Path get(WatchingFileEvent e) {
				return e.getFile();
			}
		}, 0);

	}

	private Trigger t;

	private static void registerListener() {
		if (taskID != -1) return;
		taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Skript.getInstance(), () -> {
			if (Files.exists(path)) {
				try {
					WatchService watchService = FileSystems.getDefault().newWatchService();
					WatchKey key = path.register(watchService,
							StandardWatchEventKinds.ENTRY_CREATE,
							StandardWatchEventKinds.ENTRY_DELETE,
							StandardWatchEventKinds.ENTRY_MODIFY);

					while (true) {
						final EvtWatchingFile next = new EvtWatchingFile();
						key = watchService.take();
						for (final WatchEvent<?> event : key.pollEvents()) {
							final Path file = (Path) event.context();
							next.execute(file, event.kind().name());
						}
						key.reset();
					}

				} catch (IOException | InterruptedException ex) {
					Skript.exception(ex);
				}
			}
		}, 0, 0);
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Literal<?>[] args, int matchedPattern, SkriptParser.ParseResult parseResult) {
		path = ((Literal<Path>) args[0]).getSingle();
		return true;
	}

	void execute(final Path file, final String kind) {
		final Trigger t = this.t;
		if (t == null) {
			assert false;
			return;
		}
		final WatchingFileEvent e = new WatchingFileEvent(file, kind);
		SkriptEventHandler.logEventStart(e);
		SkriptEventHandler.logTriggerEnd(t);
		t.execute(e);
		SkriptEventHandler.logTriggerEnd(t);
		SkriptEventHandler.logEventEnd();
	}

	@Override
	public void register(Trigger t) {
		this.t = t;
		registerListener();
	}

	@Override
	public void unregister(Trigger t) {
		assert t == this.t;
		this.t = null;
	}

	@Override
	public void unregisterAll() {
		if (taskID != -1)
			Bukkit.getScheduler().cancelTask(taskID);
		t = null;
		taskID = -1;
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "watching " + path;
	}
}
