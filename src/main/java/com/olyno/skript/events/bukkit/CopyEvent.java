package com.olyno.skript.events.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.nio.file.Path;

public class CopyEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	private Path source;
	private Path target;

	public CopyEvent(Path source, Path target) {
		this.source = source;
		this.target = target;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public Path getSource() {
		return source;
	}

	public Path getTarget() {
		return target;
	}
}
