package com.olyno.skript.events.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.nio.file.Path;

public class ZipEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	private Path[] source;
	private Path target;
	private String password;

	public ZipEvent(Path[] source, Path target, String password) {
		this.source = source;
		this.target = target;
		this.password = password;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public Path[] getSource() {
		return source;
	}

	public Path getTarget() {
		return target;
	}

	public String getPassword() {
		return password;
	}
}
