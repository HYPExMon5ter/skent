package com.olyno.skript.events.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.nio.file.Path;

public class RenameEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	private String newName;
	private Path file;

	public RenameEvent(Path file, String newname) {
		this.newName = newname;
		this.file = file;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public String getNewName() {
		return newName;
	}

	public Path getFile() {
		return file;
	}
}
