package com.olyno.skript.events.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.nio.file.Path;

public class DeleteEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	private Path file;

	public DeleteEvent(Path file) {
		this.file = file;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public Path getFile() {
		return file;
	}
}
