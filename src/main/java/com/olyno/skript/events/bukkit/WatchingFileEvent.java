package com.olyno.skript.events.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.nio.file.Path;

public class WatchingFileEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	private String kind;
	private Path file;

	public WatchingFileEvent(Path file, String kind) {
		this.file = file;
		this.kind = kind;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public String getKind() {
		return kind;
	}

	public Path getFile() {
		return file;
	}
}
