package com.olyno.skript.events.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.nio.file.Path;

public class CreateEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	private Path file;
	private String[] content;

	public CreateEvent(Path file, String[] content) {
		this.file = file;
		this.content = content;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public Path getFile() {
		return file;
	}

	public String[] getContent() {
		return content;
	}
}
