package com.olyno.skript.events.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.nio.file.Path;

public class DownloadEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	private String url;
	private Path file;

	public DownloadEvent(String url, Path file) {
		this.url = url;
		this.file = file;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public String getUrl() {
		return url;
	}

	public Path getFile() {
		return file;
	}
}
