package com.olyno.skript.events.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.nio.file.Path;

public class UnzipEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	private Path[] files;
	private Path source;
	private Path target;
	private String password;

	public UnzipEvent(Path[] files, Path source, Path target, String password) {
		this.files = files;
		this.source = source;
		this.target = target;
		this.password = password;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public Path getSource() {
		return source;
	}

	public Path getTarget() {
		return target;
	}

	public String getPassword() {
		return password;
	}

	public Path[] getFiles() {
		return files;
	}
}
