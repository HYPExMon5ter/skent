package com.olyno.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.olyno.skript.events.bukkit.RenameEvent;

import java.nio.file.Path;

@Name("Rename File/Directory")
@Description("When a file or directory is renamed.")
@Examples({
		""
})
@Since("1.0")

public class EvtRename {

	static {
		Skript.registerEvent("Rename", SimpleEvent.class, RenameEvent.class,
				"[(file|dir[ector(ies|y)])] rename[d]"
		);

		EventValues.registerEventValue(RenameEvent.class, String.class, new Getter<String, RenameEvent>() {
			@Override
			public String get(RenameEvent e) {
				return e.getNewName();
			}
		}, 0);

		EventValues.registerEventValue(RenameEvent.class, Path.class, new Getter<Path, RenameEvent>() {
			@Override
			public Path get(RenameEvent e) {
				return e.getFile();
			}
		}, 0);

	}
}
