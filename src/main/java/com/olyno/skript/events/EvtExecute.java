package com.olyno.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.olyno.skript.events.bukkit.ExecuteEvent;

import java.nio.file.Path;

@Name("Execute File/Directory")
@Description("When a file or directory is executed.")
@Examples({
		""
})
@Since("1.0")

public class EvtExecute {

	static {
		Skript.registerEvent("Execute", SimpleEvent.class, ExecuteEvent.class,
				"[(file|dir[ector(ies|y)])] execute[d]"
		);

		EventValues.registerEventValue(ExecuteEvent.class, Path.class, new Getter<Path, ExecuteEvent>() {
			@Override
			public Path get(ExecuteEvent e) {
				return e.getFile();
			}
		}, 0);

	}
}
