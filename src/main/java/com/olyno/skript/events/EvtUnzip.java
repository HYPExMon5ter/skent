package com.olyno.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.olyno.skript.events.bukkit.UnzipEvent;

import java.nio.file.Path;

@Name("Unzip File/Directory")
@Description("When files are unzipped.")
@Examples({
		""
})
@Since("1.0")

public class EvtUnzip {

	static {
		Skript.registerEvent("Unzip", SimpleEvent.class, UnzipEvent.class,
				"[(file|dir[ector(ies|y)])] unzip[ed]"
		);

		EventValues.registerEventValue(UnzipEvent.class, Path.class, new Getter<Path, UnzipEvent>() {
			@Override
			public Path get(UnzipEvent e) {
				return e.getTarget();
			}
		}, 0);

		EventValues.registerEventValue(UnzipEvent.class, Path.class, new Getter<Path, UnzipEvent>() {
			@Override
			public Path get(UnzipEvent e) {
				return e.getSource();
			}
		}, 0);

		EventValues.registerEventValue(UnzipEvent.class, String.class, new Getter<String, UnzipEvent>() {
			@Override
			public String get(UnzipEvent e) {
				return e.getPassword();
			}
		}, 0);

	}
}
