package com.olyno.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.olyno.skript.events.bukkit.ZipEvent;

import java.nio.file.Path;

@Name("Zip File/Directory")
@Description("When files or directories are zipped.")
@Examples({
		""
})
@Since("1.0")

public class EvtZip {

	static {
		Skript.registerEvent("Zip", SimpleEvent.class, ZipEvent.class,
				"[(file|dir[ector(ies|y)])] zip[ed]"
		);

		EventValues.registerEventValue(ZipEvent.class, Path.class, new Getter<Path, ZipEvent>() {
			@Override
			public Path get(ZipEvent e) {
				return e.getTarget();
			}
		}, 0);

		EventValues.registerEventValue(ZipEvent.class, String.class, new Getter<String, ZipEvent>() {
			@Override
			public String get(ZipEvent e) {
				return e.getPassword();
			}
		}, 0);

	}
}
