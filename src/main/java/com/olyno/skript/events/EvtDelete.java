package com.olyno.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.olyno.skript.events.bukkit.DeleteEvent;

import java.nio.file.Path;

@Name("Delete File/Directory")
@Description("When a file or directory is deleted.")
@Examples({
		""
})
@Since("1.0")

public class EvtDelete {

	static {
		Skript.registerEvent("Delete", SimpleEvent.class, DeleteEvent.class,
				"[(file|dir[ector(ies|y)])] delet(e[d]|ion)"
		);

		EventValues.registerEventValue(DeleteEvent.class, Path.class, new Getter<Path, DeleteEvent>() {
			@Override
			public Path get(DeleteEvent e) {
				return e.getFile();
			}
		}, 0);

	}
}
