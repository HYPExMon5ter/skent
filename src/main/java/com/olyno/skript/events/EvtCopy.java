package com.olyno.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.util.SimpleEvent;
import ch.njol.skript.registrations.EventValues;
import ch.njol.skript.util.Getter;
import com.olyno.skript.events.bukkit.CopyEvent;

import java.nio.file.Path;

@Name("Copy File/Directory")
@Description("When a file or directory is copied.")
@Examples({
		""
})
@Since("1.0")

public class EvtCopy {

	static {
		Skript.registerEvent("Copy", SimpleEvent.class, CopyEvent.class,
				"[(file|dir[ector(ies|y)])] cop(y|ied)"
		);

		EventValues.registerEventValue(CopyEvent.class, Path.class, new Getter<Path, CopyEvent>() {
			@Override
			public Path get(CopyEvent e) {
				return e.getSource();
			}
		}, 0);

		EventValues.registerEventValue(CopyEvent.class, Path.class, new Getter<Path, CopyEvent>() {
			@Override
			public Path get(CopyEvent e) {
				return e.getTarget();
			}
		}, 0);

	}
}
