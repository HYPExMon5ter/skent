package com.olyno.skript.expressions.files;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.SimplePropertyExpression;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Name("Owner of File/Directory")
@Description("Returns the owner of a file or directory.")
@Examples({
	"command owner:\n" +
			"\ttrigger:\n" +
			"\t\tset {_owner} to owner of file \"plugins/Skript/config.sk\"\n" +
			"\t\tbroadcast \"Hey boi, look I'm my computer is the owner: %{_owner}%\""
})
@Since("1.0")

public class ExprOwner extends SimplePropertyExpression<Path, String> {

	static {
		register(ExprOwner.class, String.class,
				"[the] owner", "path"
		);
	}

	@Override
	public String convert(Path file) {
		if (Files.exists(file)) {
			try {
				return Files.getOwner(file).getName();
			} catch (IOException ex) {
				Skript.exception(ex, "This file doesn't exist: " + file);
			}
		}
		return null;
	}

	@Override
	protected String getPropertyName() {
		return "owner";
	}

	@Override
	public Class<? extends String> getReturnType() {
		return String.class;
	}
}
