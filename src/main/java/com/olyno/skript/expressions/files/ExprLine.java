package com.olyno.skript.expressions.files;

import ch.njol.skript.Skript;
import ch.njol.skript.classes.Changer.ChangeMode;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Name("Line of file")
@Description("Returns a specific line of a file.")
@Examples({
		"command line:\n" +
			"\ttrigger:\n" +
				"\t\tset line 1 of file \"plugins/Skript/scripts/myAwesomeScript.sk\" to \"command awesometest\"\n" +
				"\t\tadd \":\" to line 1 of file \"plugins/Skript/scripts/myAwesomeScript.sk\"\n" +
				"\t\tremove \"test\" from line 1 of file \"plugins/Skript/scripts/myAwesomeScript.sk\"\n" +
				"\t\tbroadcast line 1 of file \"plugins/Skript/scripts/myAwesomeScript.sk\""
})
@Since("1.0")

public class ExprLine extends SimpleExpression<String> {

	static {
		Skript.registerExpression(ExprLine.class, String.class, ExpressionType.SIMPLE,
				"[the] line %integer% (from|of|in) %path%",
				"[all] [the] lines (from|of|in) %path%"
		);
	}

	private Expression<Integer> line;
	private Expression<Path> file;
	private Path path;
	private Integer theLine;
	private Boolean allLines = false;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		if (matchedPattern == 0) {
			line = (Expression<Integer>) expr[0];
			file = (Expression<Path>) expr[1];
		} else {
			file = (Expression<Path>) expr[0];
			allLines = true;
		}
		return true;
	}

	@Override
	protected String[] get(Event e) {
		path = file.getSingle(e);
		theLine = line != null ? line.getSingle(e) : 0;
		if (Files.exists(path)) {
			try {
				if (allLines) return Files.readAllLines(path).toArray(new String[0]);
				return new String[]{Files.lines(path).skip(theLine - 1).findFirst().get()};
			} catch (IOException ex) {
				Skript.exception(ex, "This file doesn't exist: " + path);
			}
		}
		return null;
	}

	@Override
	public Class<?>[] acceptChange(final ChangeMode mode) {
		switch (mode){
			case SET:
			case ADD:
			case REMOVE:
				return new Class[]{String.class};
		}
		return null;
	}

	@Override
	public void change( Event e, Object[] delta, ChangeMode mode) {
		try {
			List<String> lines = Files.readAllLines(file.getSingle(e), StandardCharsets.UTF_8);
			switch (mode) {

				case SET:
					lines.set(theLine - 1, (String) delta[0]);
					break;

				case ADD:
					lines.set(theLine - 1, lines.get(theLine - 1) + delta[0]);
					break;

				case REMOVE:
					lines.set(theLine - 1, lines.get(theLine - 1).replaceAll((String) delta[0], ""));
					break;
			}
			Files.write(path, lines, StandardCharsets.UTF_8);

		} catch (IOException ex) {
			Skript.exception(ex, "This file doesn't exist: " + path);
		}

	}

	@Override
	public boolean isSingle() {
		return (!allLines);
	}

	@Override
	public Class<? extends String> getReturnType() {
		return String.class;
	}

	@Override
	public String toString(Event e, boolean debug) {
		return (allLines ? "all lines" : "line " + line.toString(e, debug)) + " of " + file.toString(e, debug);
	}
}
