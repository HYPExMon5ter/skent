package com.olyno.skript.expressions.files;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedList;
import java.util.List;

@Name("All File/Directory")
@Description("Returns all files or directories in a dir.")
@Examples({
	"command allFiles:\n" +
			"\ttrigger:\n" +
			"\t\tset {_files::*} to all files of \"plugins/Skript\"\n" +
			"\t\tbroadcast \"Wow Skript has %amount of {_files::*}% files: %{_files::*}%\""
})
@Since("1.0")

public class ExprAllFileDirectory extends SimpleExpression<Path> implements FileVisitor<Path> {

	static {
		Skript.registerExpression(ExprAllFileDirectory.class, Path.class, ExpressionType.SIMPLE,
				"all [the] dir[ector(y|ie)]s (in|of) %path% (including|with) [all] sub[(-| )]dir[ectorie]s and [all] sub(-| )files",
				"all [the] dir[ector(y|ie)]s (in|of) %path% (including|with) [all] sub[(-| )]dir[ectorie]s",
				"all [the] dir[ector(y|ie)]s (in|of) %path% (including|with) [all] sub[(-| )]files",
				"all [the] dir[ector(y|ie)]s (in|of) %path%",
				"all [the] files (in|of) %path%"
		);
	}

	private Expression<Path> file;
	private Path parent;
	private Boolean allFiles;
	private Boolean includingSubDirs;
	private Boolean includingSubFiles;
	private Boolean includingSubDirsAndFiles;
	private List<Path> files = new LinkedList<>();

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		file = (Expression<Path>) expr[0];
		includingSubDirsAndFiles = matchedPattern == 0;
		includingSubDirs = matchedPattern == 1;
		includingSubFiles = matchedPattern == 2;
		allFiles = matchedPattern == 4;
		return true;
	}

	@Override
	protected Path[] get(Event e) {
		Path path = file.getSingle(e);
		try {
			if (Files.exists(path)) {
				if (Files.isDirectory(path)) {
					parent = path;
				} else {
					parent = path.getParent();
				}
			}
			Files.walkFileTree(path, this);
			return files.toArray(new Path[0]);
		} catch (IOException ex) {
			Skript.exception(ex);
		}
		return null;
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		if (!dir.equals(parent)) {
			if (!allFiles) {
				if (includingSubDirsAndFiles || includingSubDirs) {
					files.add(dir);
					return FileVisitResult.CONTINUE;
				} else if (parent.equals(dir.getParent())) {
					files.add(dir);
					return FileVisitResult.SKIP_SUBTREE;
				} else {
					return FileVisitResult.TERMINATE;
				}
			}
			return FileVisitResult.SKIP_SUBTREE;
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		if (allFiles) {
			if (includingSubDirsAndFiles || includingSubFiles || parent.equals(file.getParent())) {
				files.add(file);
			}
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		if (includingSubDirsAndFiles || includingSubDirs || includingSubFiles || dir.equals(parent))
			return FileVisitResult.CONTINUE;
		return FileVisitResult.TERMINATE;
	}

	@Override
	public boolean isSingle() {
		return false;
	}

	@Override
	public Class<? extends Path> getReturnType() {
		return Path.class;
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "all " + (allFiles ? " files " : " directories ") + "in " + file.toString(e, debug);
	}
}
