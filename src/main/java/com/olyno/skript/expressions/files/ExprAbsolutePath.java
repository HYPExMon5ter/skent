package com.olyno.skript.expressions.files;

import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.SimplePropertyExpression;

import java.nio.file.Path;

@Name("Absolute path")
@Description("Returns the absolute path of a file.")
@Examples({
	"command absolute:\n" +
			"\ttrigger:\n" +
			"\t\tset {_path} to absolute path of file \"plugins/Skript/config.sk\"\n" +
			"\t\tbroadcast \"The path of the config file is %{_path}%\""
})
@Since("1.0")

public class ExprAbsolutePath extends SimplePropertyExpression<Path, String> {

	static {
		register(ExprAbsolutePath.class, String.class,
				"[the] absolute path", "path"
		);
	}


	@Override
	public String convert(Path file) {
		return file.toAbsolutePath().toString();
	}

	@Override
	protected String getPropertyName() {
		return "absolute path";
	}

	@Override
	public Class<? extends String> getReturnType() {
		return String.class;
	}
}
