package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.skript.events.bukkit.DownloadEvent;
import com.olyno.util.AsyncEffect;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;

@Name("Download File")
@Description("Downloads a file from a url.")
@Examples({
	"command download:\n" +
			"\ttrigger:\n" +
			"\t\tdownload from url \"https://bitbucket.org/AlexLew95/skent/downloads/Skent-1.0-all.jar\" to file \"plugins/Skent.jar\"\n" +
			"\t\tbroadcast \"Skent downloaded!\""
})
@Since("1.0")

public class EffDownloadFile extends AsyncEffect {

	static {
		Skript.registerEffect(EffDownloadFile.class,
				"download [file] from [url] %string% to %path%"
		);
	}

	private Expression<String> url;
	private Expression<Path> file;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		url = (Expression<String>) expr[0];
		file = (Expression<Path>) expr[1];
		return true;
	}

	@Override
	protected void execute(Event e) {
		download(url.getSingle(e), file.getSingle(e));
	}

	private void download(String url, Path output) {
		try {
			ReadableByteChannel rbc = Channels.newChannel(new URL(url).openStream());
			FileOutputStream fos = new FileOutputStream(output.toString());
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
			rbc.close();
			Bukkit.getServer().getPluginManager().callEvent(new DownloadEvent(url, output));

		} catch (IOException ex) {
			Skript.exception(ex);
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "download file from " + url.toString(e, debug) + " to file " + file.toString(e, debug);
	}
}
