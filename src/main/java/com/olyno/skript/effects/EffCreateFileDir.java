package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.skript.events.bukkit.CreateEvent;
import com.olyno.util.AsyncEffect;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@Name("Create File or directory")
@Description("Creates a file or a directory. If the file exists, then it will be replaced by the new one.")
@Examples({
	"command create:\n" +
			"\ttrigger:\n" +
			"\t\tcreate file \"plugins/Skript/scripts/myAwesomeScript.sk\" with text \"command awesome:\", \"\ttrigger:\" and \"\t\tbroadcast \"\"Awesome!!!\"\"\"\n" +
			"\t\tbroadcast \"Created!\""
})
@Since("1.0")

public class EffCreateFileDir extends AsyncEffect {

	static {
		Skript.registerEffect(EffCreateFileDir.class,
				"create %path% with [(text|string|content)] %strings%",
				"create %path%"
		);
	}

	private Expression<Path> file;
	private Expression<String> content;
	private Boolean withContent = false;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		file = (Expression<Path>) expr[0];
		if (matchedPattern == 0) {
			content = (Expression<String>) expr[1];
			withContent = true;
		}
		return true;
	}

	@Override
	protected void execute(Event e) {
		Path path = file.getSingle(e);
		String[] currentContent = content != null ? content.getArray(e) : new String[]{""};
		try {
			if (Pattern.compile("\\.\\w+$").matcher(path.toString()).find()) {
				if (withContent) {
					List<String> lines = Arrays.asList(currentContent);
					Files.write(path, lines, Charset.forName("UTF-8"));
				} else {
					Files.createFile(path);
				}
			} else {
				Files.createDirectory(path);
			}
			Bukkit.getServer().getPluginManager().callEvent(new CreateEvent(path, currentContent));
		} catch (IOException ex) {
			if (Files.exists(path)) {
				Skript.exception(ex, "This file/directory already exists: " + path);
			} else {
				Skript.exception(ex);
			}

		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "create " + file.toString(e, debug);
	}
}
