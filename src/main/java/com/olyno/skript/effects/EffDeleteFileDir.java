package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.skript.events.bukkit.DeleteEvent;
import com.olyno.util.AsyncEffect;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Name("Delete File or directory")
@Description("Deletes a file or a directory.")
@Examples({
	"command delete:\n" +
			"\ttrigger:\n" +
			"\t\tdelete file \"plugins/Skript/scripts/myAwesomeScript.sk\"\n" +
			"\t\tbroadcast \"Oh no, my awesome script!\""
})
@Since("1.0")

public class EffDeleteFileDir extends AsyncEffect {

	static {
		Skript.registerEffect(EffDeleteFileDir.class,
				"delete %path%"
		);
	}

	private Expression<Path> file;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		file = (Expression<Path>) expr[0];
		return true;
	}

	@Override
	protected void execute(Event e) {
		Path path = file.getSingle(e);
		try {
			Files.deleteIfExists(path);
			Bukkit.getServer().getPluginManager().callEvent(new DeleteEvent(path));
		} catch (IOException ex) {
			if (Files.exists(path)) {
				Skript.exception(ex, "This file/directory doesn't exist: " + path);
			} else {
				Skript.exception(ex);
			}
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "delete " + file.toString(e, debug);
	}
}
