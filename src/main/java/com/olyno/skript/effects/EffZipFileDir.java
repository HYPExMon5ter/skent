package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.skript.events.bukkit.ZipEvent;
import com.olyno.util.AsyncEffect;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;

@Name("Zip File or Directory")
@Description("Zips files and/or directories.")
@Examples({
	"command zip:\n" +
			"\ttrigger:\n" +
			"\t\tzip dir \"plugins/Skript/scripts\" to dir \"plugins/Skript/scripts.zip\"\n" +
			"\t\tbroadcast \"Nice I did a backup!\""
})
@Since("1.0")

public class EffZipFileDir extends AsyncEffect {

	static {
		Skript.registerEffect(EffZipFileDir.class,
				"zip %paths% to %path% with [password] %string%",
				"zip %paths% to %path%"
		);
	}

	private Expression<Path> source;
	private Expression<Path> target;
	private Expression<String> password;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		source = (Expression<Path>) expr[0];
		target = (Expression<Path>) expr[1];
		if (matchedPattern == 0) {
			password = (Expression<String>) expr[2];
		}
		return true;
	}

	@Override
	protected void execute(Event e) {
		Path[] sourceFile = source.getArray(e);
		Path targetFile = target.getSingle(e);
		String pass = password.getSingle(e);
		if (Pattern.compile("\\.zip$").matcher(targetFile.toString()).find()) {
			if (password != null && pass != null) {
				zip(sourceFile, targetFile, pass);
			} else {
				zip(sourceFile, targetFile, null);
			}
		}
	}

	private void zip(Path[] src, Path dest, String pass) {
		try {
			ZipFile zipFile = new ZipFile(dest.toString());
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);

			if (pass != null) {
				parameters.setEncryptFiles(true);
				parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
				parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_128);
				parameters.setPassword(pass);
			}

			for (Path file : src) {
				if (Files.exists(file)) {
					if (Files.isDirectory(file)) {
						zipFile.addFolder(file.toString(), parameters);
					} else {
						zipFile.addFile(file.toFile(), parameters);
					}
				}
			}
			Bukkit.getServer().getPluginManager().callEvent(new ZipEvent(src, dest, pass));
		} catch (ZipException ex) {
			Skript.exception(ex);
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "zip " + source.toString(e, debug) + " to " + target.toString(e, debug);
	}
}
