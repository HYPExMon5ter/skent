package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.skript.events.bukkit.ExecuteEvent;
import com.olyno.util.AsyncEffect;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Name("Run/Execute File")
@Description("Runs/Executes a file.")
@Examples({
	"command execute:\n" +
			"\ttrigger:\n" +
			"\t\texecute file \"plugins/myAwesomeBat.bat\"\n" +
			"\t\tbroadcast \"File executed! I'm now a hacker!\""
})
@Since("1.0")

public class EffExecuteFile extends AsyncEffect {

	static {
		Skript.registerEffect(EffExecuteFile.class,
				"(execute|run) %path%"
		);
	}

	private Expression<Path> file;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		file = (Expression<Path>) expr[0];
		return true;
	}

	@Override
	protected void execute(Event e) {
		launchApp(file.getSingle(e));
	}

	private void launchApp(Path app) {
		if (Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
			try {
				Desktop.getDesktop().open(app.toFile());
				Bukkit.getServer().getPluginManager().callEvent(new ExecuteEvent(app));
			} catch (IOException ex) {
				if (Files.exists(app)) {
					Skript.exception(ex, "This file doesn't exist: " + app);
				} else {
					Skript.exception(ex);
				}
			}
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "execute " + file.toString(e, debug);
	}
}
