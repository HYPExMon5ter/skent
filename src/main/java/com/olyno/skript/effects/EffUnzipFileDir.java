package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.skript.events.bukkit.UnzipEvent;
import com.olyno.util.AsyncEffect;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;

@Name("Unzip File or Directory")
@Description("Unzips files and/or directories.")
@Examples({
	"command unzip:\n" +
			"\ttrigger:\n" +
			"\t\tunzip all files in file \"plugins/Skript/scripts.zip\" to dir \"plugins/Skript\"\n" +
			"\t\tbroadcast \"Nice I retrieved my backup!\""
})
@Since("1.0")

public class EffUnzipFileDir extends AsyncEffect {

	static {
		Skript.registerEffect(EffUnzipFileDir.class,
				"(unzip|extract) all files (in|of|from) %path% to %path% with [password] %string%",
				"(unzip|extract) all files (in|of|from) %path% to %path%",
				"(unzip|extract) %path% (in|of|from) %path% to %path% with [password] %string%",
				"(unzip|extract) %path% (in|of|from) %path% to %path%"
		);
	}

	private Expression<Path> files;
	private Expression<Path> source;
	private Expression<Path> target;
	private Expression<String> password;
	private Boolean allFiles = false;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		int index = 0;
		if (matchedPattern == 0 || matchedPattern == 1) {
			allFiles = true;
			index ++;
		} else {
			files = (Expression<Path>) expr[0];
		}
		source = (Expression<Path>) expr[1 - index];
		target = (Expression<Path>) expr[2 - index];
		if (matchedPattern == 0 || matchedPattern == 2) {
			password = (Expression<String>) expr[3 - index];
		}
		return true;
	}

	@Override
	protected void execute(Event e) {
		if (!Pattern.compile("\\.zip$").matcher(source.getSingle(e).toString()).find()) return;
		unzip(
				files != null ? files.getArray(e) : null,
				source.getSingle(e),
				target.getSingle(e),
				password != null ? password.getSingle(e) : null
		);

	}


	private void unzip(Path[] allFiles, Path src, Path dest, String pass) {
		try {
			if (!Files.exists(dest)) Files.createDirectory(dest);
			ZipFile zipFile = new ZipFile(src.toString());
			if (zipFile.isEncrypted()) {
				if (pass != null) {
					zipFile.setPassword(pass);
				}
			}

			if (allFiles == null) {
				zipFile.extractAll(dest.toString());
			} else {
				for (Path file : allFiles) {
					zipFile.extractFile(file.toString(), dest.toString());
				}
			}
			Bukkit.getServer().getPluginManager().callEvent(new UnzipEvent(allFiles, src, dest, pass));

		} catch (ZipException | IOException ex) {
			Skript.exception(ex);
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "unzip " + (allFiles ? " all files " : files.toString(e, debug)) + "in " + source.toString(e, debug) + " to " + target.toString(e, debug) + (password != null ? " with password " + password.toString(e, debug) : "");
	}
}
