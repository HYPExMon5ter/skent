package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.util.AsyncEffect;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

@Name("Appends Text File")
@Description("Appends any text to a file.")
@Examples({
	"command append:\n" +
			"\ttrigger:\n" +
			"\t\tcreate file \"plugins/Skript/scripts/test1.txt\" with \"My name is\"\n" +
			"\t\tappend \"secret\" to file \"plugins/Skript/scripts/test1.txt\"\n" +
			"\t\tbroadcast \"The text has been added!\""
})
@Since("1.0")

public class EffAppendFile extends AsyncEffect {

	static {
		Skript.registerEffect(EffAppendFile.class,
				"append %strings% to %path%"
		);
	}

	private Expression<String> content;
	private Expression<Path> file;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		content = (Expression<String>) expr[0];
		file = (Expression<Path>) expr[1];
		return true;
	}

	@Override
	protected void execute(Event e) {
		Path path = file.getSingle(e);
		try {
			Files.write(
					path,
					Arrays.asList(content.getArray(e)),
					Charset.forName("UTF-8"), StandardOpenOption.APPEND
			);

		} catch (IOException ex) {
			if (Files.exists(path)) {
				Skript.exception(ex, "This file/directory doesn't exist: " + path);
			} else {
				Skript.exception(ex);
			}
		}

	}

	@Override
	public String toString(Event e, boolean debug) {
		return "append " + content.toString(e, debug) + " to " + file.toString(e, debug);
	}
}
