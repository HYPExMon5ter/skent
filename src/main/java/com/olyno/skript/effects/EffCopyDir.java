package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.skript.events.bukkit.CopyEvent;
import com.olyno.util.AsyncEffect;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Name("Copy File or directory")
@Description("Copy a file or a directory to another.")
@Examples({
	"command copy:\n" +
			"\ttrigger:\n" +
			"\t\tcopy file \"plugins/Skript/scrips/MyAwesomeScript.sk\" to file \"plugins/Skript/scrips/MyAwesomeScriptCopy.sk\"\n" +
			"\t\tbroadcast \"Copied!\""
})
@Since("1.0")

public class EffCopyDir extends AsyncEffect {

	static {
		Skript.registerEffect(EffCopyDir.class,
				"copy %path% to %path% with replace",
				"copy %path% to %path%"
		);
	}

	private Expression<Path> source;
	private Expression<Path> target;
	private Boolean withReplace;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		source = (Expression<Path>) expr[0];
		target = (Expression<Path>) expr[1];
		withReplace = matchedPattern == 0;
		return true;
	}

	@Override
	protected void execute(Event e) {
		Path sourceFile = source.getSingle(e);
		Path targetFile = target.getSingle(e);
		try {
			if (Files.isDirectory(sourceFile)) {
				copyFolder(sourceFile, targetFile);
			} else if (withReplace) {
				Files.copy(sourceFile, targetFile, StandardCopyOption.REPLACE_EXISTING);
			} else {
				Files.copy(sourceFile, targetFile);
			}
			Bukkit.getServer().getPluginManager().callEvent(new CopyEvent(source.getSingle(e), target.getSingle(e)));
		} catch (IOException ex) {
			if (!Files.exists(sourceFile)) {
				Skript.exception(ex, "Source file or directory doesn't exist.");
			} else if (Files.exists(targetFile)) {
				Skript.exception(ex, "Target file or directory already exist.");
			} else {
				Skript.exception(ex);
			}
		}
	}

	private void copyFolder(Path src, Path dest) throws IOException {
		Files.walk(src)
				.forEach(source -> copy(source, dest.resolve(src.relativize(source))));
	}

	private void copy(Path source, Path dest) {
		try {
			if (withReplace) {
				Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
			} else {
				Files.copy(source, dest);
			}
		} catch (Exception ex) {
			Skript.exception(ex);
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "copy " + source.toString(e, debug) + " to " + target.toString(e, debug);
	}
}
