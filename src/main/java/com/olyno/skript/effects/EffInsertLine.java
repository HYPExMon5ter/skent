package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.util.AsyncEffect;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Name("Insert Line")
@Description("Inserts a line in a file.")
@Examples({
	"command insert:\n" +
			"\ttrigger:\n" +
			"\t\tcreate file \"plugins/Skript/scripts/test1.txt\" with text \"Hey\", \"I'm\" and \"nice to meet you!\"\n" +
			"\t\tinsert \"the creator\" at line 2 of file \"plugins/Skript/scripts/test1.txt\"\n" +
			"\t\tbroadcast \"Of course I'm the creator!\""
})
@Since("1.0")

public class EffInsertLine extends AsyncEffect {

	static {
		Skript.registerEffect(EffInsertLine.class,
				"insert %strings% at line %integer% of %path%"
		);
	}

	private Expression<String> content;
	private Expression<Integer> line;
	private Expression<Path> file;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		content = (Expression<String>) expr[0];
		line = (Expression<Integer>) expr[1];
		file = (Expression<Path>) expr[2];
		return true;
	}

	@Override
	protected void execute(Event e) {
		Path path = file.getSingle(e);
		try {
			List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
			lines.add(line.getSingle(e), content.getSingle(e));
			Files.write(path, lines, StandardCharsets.UTF_8);
		} catch (IOException ex) {
			if (Files.exists(path)) {
				Skript.exception(ex, "This file doesn't exist: " + path);
			} else {
				Skript.exception(ex);
			}
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "insert \"" + content.toString(e, debug) + "\" at line " + line.toString(e, debug) + " of " + file.toString(e, debug);
	}
}
