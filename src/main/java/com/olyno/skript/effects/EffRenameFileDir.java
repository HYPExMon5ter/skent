package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.skript.events.bukkit.RenameEvent;
import com.olyno.util.AsyncEffect;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Name("Rename File or directory")
@Description("Renames a file or a directory.")
@Examples({
	"command rename:\n" +
			"\ttrigger:\n" +
			"\t\trename file \"plugins/Skript/scripts/test1.txt\" to \"secret.txt\"\n" +
			"\t\tbroadcast \"Now my file is secret!\""
})
@Since("1.0")

public class EffRenameFileDir extends AsyncEffect {

	static {
		Skript.registerEffect(EffRenameFileDir.class,
				"rename %path% to %string% with replace",
				"rename %path% to %string%"
		);
	}

	private Expression<Path> source;
	private Expression<String> name;
	private Boolean withReplace;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		source = (Expression<Path>) expr[0];
		name = (Expression<String>) expr[1];
		withReplace = matchedPattern == 0;
		return true;
	}

	@Override
	protected void execute(Event e) {
		Path currentSource = source.getSingle(e);
		String currentName = name.getSingle(e);
		try {
			if (withReplace) {
				Files.move(currentSource, currentSource.resolveSibling(currentName), StandardCopyOption.REPLACE_EXISTING);
			} else {
				Files.move(currentSource, currentSource.resolveSibling(currentName));
			}
			Bukkit.getServer().getPluginManager().callEvent(new RenameEvent(currentSource, currentName));
		} catch (IOException ex) {
			if (Files.exists(currentSource)) {
				Skript.exception(ex, "This file/directory doesn't exist: " + currentSource);
			} else {
				Skript.exception(ex);
			}
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "rename " + source.toString(e, debug) + " to " + name.toString(e, debug) + (withReplace ? " with replace" : " without replace");
	}
}
