package com.olyno.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.olyno.skript.events.bukkit.MoveEvent;
import com.olyno.util.AsyncEffect;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Name("Move File or directory")
@Description("Moves a file or a directory.")
@Examples({
	"command move:\n" +
			"\ttrigger:\n" +
			"\t\tmove file \"plugins/Skript/config.sk\" to file \"plugins/config.sk\"\n" +
			"\t\tbroadcast \"Got a new config!\""
})
@Since("1.0")

public class EffMoveFileDir extends AsyncEffect {

	static {
		Skript.registerEffect(EffMoveFileDir.class,
				"move %path% to %path% with replace",
				"move %path% to %path%"
		);
	}

	private Expression<Path> source;
	private Expression<Path> target;
	private Boolean withReplace = false;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		source = (Expression<Path>) expr[0];
		target = (Expression<Path>) expr[1];
		withReplace = matchedPattern == 0;
		return true;
	}

	@Override
	protected void execute(Event e) {
		Path sourceFile = source.getSingle(e);
		Path targetFile = target.getSingle(e);
		try {
			if (withReplace) {
				Files.move(sourceFile, targetFile, StandardCopyOption.REPLACE_EXISTING);
			} else {
				Files.move(sourceFile, targetFile);
			}
			Bukkit.getServer().getPluginManager().callEvent(new MoveEvent(sourceFile, targetFile));
		} catch (IOException ex) {
			Skript.exception(ex);
		}
	}

	@Override
	public String toString(Event e, boolean debug) {
		return "move " + source.toString(e, debug) + " to " + target.toString(e, debug);
	}
}
